#include <iostream>
#include <stdlib.h>
#include <string>
#include <time.h>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;

int main() {
    // Generating random number and converting it to string
    srand(time(0));
    int num;
    num = rand() % 898 + 1;
    std::string numstr;
    numstr = std::to_string(num);

    std::cout << "Catching pokemon\n";
    // Getting random pokemon information from pokeapi
    RestClient::Response r = RestClient::get(
"https://pokeapi.co/api/v2/pokemon/"+numstr);

    std::cout << "\n";
    // Printing code to see if valid response is recieved
    std::cout << "Response code: " << r.code << "\n";
    // parsing data and printing the relevant information
    auto json_body = json::parse(r.body);
    std::cout<< json_body["name"] <<"\n";
    std::cout<< json_body["sprites"]["front_default"] <<"\n";
    std::cout<< json_body["types"][0]["type"]["name"] <<"\n";
    if(json_body["types"][1]["type"]["name"] != nullptr){
        std::cout<< json_body["types"][1]["type"]["name"] <<"\n";        
    }

    // Preparing data in correct format to send to server
    json pokemon;
    pokemon["name"] = json_body["name"];
    pokemon["sprites"] = json_body["sprites"]["front_default"];
    pokemon["type1"] = json_body["types"][0]["type"]["name"];
    pokemon["type2"] = json_body["types"][1]["type"]["name"];
    pokemon.dump();
    
    json data;
    data["data"] = pokemon.dump();
    data["meta"] = "pokemon";
    data.dump();

    // fetching all data of pokemon already on the server
    RestClient::Response c = RestClient::get(
    "localhost:5000/fetch/all");
    // parsing the fetched data
    auto fetch_body = json::parse(c.body);
    int count = fetch_body.size();
    std::string pokemonData;// String to hold the data later
    
    // Iterating through the recieved pokemon data
    for(int i = 0; i < count; i++){
        // Converting data to string
        pokemonData = fetch_body[i]["data"];
        // String back to json
        auto parsedPokemon = json::parse(pokemonData);
        // If pokemon exists already, program ends
        if(parsedPokemon["name"] == pokemon["name"]){
            std::cout<<"Pokemon already found, not added."<<"\n";
            return 0;
        }
        
    }
    // send pokemon to server if it does not exist
    RestClient::Response p = RestClient::post(
    "http:/localhost:5000/submit", "application/json", data.dump());
    std::cout << "Response code: " << p.code << "\n";


    return 0;
}